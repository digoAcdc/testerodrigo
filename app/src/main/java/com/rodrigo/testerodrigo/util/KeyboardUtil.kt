package com.rodrigo.testerodrigo.util

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

class KeyboardUtil {
    companion object {
        fun hideKeyboard(activity: Activity, callback: () -> Unit = {}) {
            val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(activity!!.findViewById<View>(android.R.id.content).getWindowToken(), 0)

           /* GlobalScope.launch {
                delay(300)
                activity.runOnUiThread {
                    callback()
                }
            }*/
        }
    }
}