package com.rodrigo.testerodrigo.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataGit(val owner: Owner) : Parcelable
