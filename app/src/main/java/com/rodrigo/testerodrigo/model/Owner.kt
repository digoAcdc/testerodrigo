package com.rodrigo.testerodrigo.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
class Owner(@PrimaryKey val id: Long, val login : String, val avatar_url : String) : Parcelable

