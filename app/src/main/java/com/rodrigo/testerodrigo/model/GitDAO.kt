package com.rodrigo.testerodrigo.model

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface GitDAO {

    @Query("SELECT * FROM owner")
    fun getAllGits(): List<Owner>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGit(owner: Owner)

    @Query("DELETE FROM owner WHERE id = :gitId")
      fun deleteByUserId(gitId: Long)
}