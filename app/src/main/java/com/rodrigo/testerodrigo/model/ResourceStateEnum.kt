package com.rodrigo.testerodrigo.model

enum class ResourceStateEnum {
    LOADING, SUCCESS, ERROR
}