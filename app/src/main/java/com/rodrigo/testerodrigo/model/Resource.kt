package com.rodrigo.testerodrigo.model

class Resource<out T> constructor(val status: ResourceStateEnum, val data: T? = null, val message: String? = null) {

    companion object {

        fun <T> loading(): Resource<T> = Resource(ResourceStateEnum.LOADING)

        fun <T> success(data: T?) : Resource<T> = Resource(ResourceStateEnum.SUCCESS, data)

        fun <T> success(data: T?, message: String?) : Resource<T> = Resource(ResourceStateEnum.SUCCESS, data, message)

        fun <T> success() : Resource<T> = Resource(ResourceStateEnum.SUCCESS)

        fun <T> error(message: String?) : Resource<T> = Resource(ResourceStateEnum.ERROR, message = message)

    }
}