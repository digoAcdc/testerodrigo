package com.rodrigo.testerodrigo.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rodrigo.testerodrigo.R
import com.rodrigo.testerodrigo.model.Loading
import com.rodrigo.testerodrigo.model.Owner
import com.squareup.picasso.Picasso
import com.subinkrishna.widget.CircularImageView


class GitAdapter(val onClickListener: GitItemOnClickListener?) :
    RecyclerView.Adapter<GitAdapter.CustomViewHolder>() {


    private var myDataset: MutableList<Any?> = ArrayList()

    interface GitItemOnClickListener {

        fun OnClickItemListener(view: View, position: Int)

    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GitAdapter.CustomViewHolder {

        var root: View? = null
        if (viewType == R.layout.item_git) {
            root = LayoutInflater.from(parent.context).inflate(R.layout.item_git, parent, false)
            return ViewHolder(root)
        } else {
            root = LayoutInflater.from(parent.context).inflate(R.layout.item_loading, parent, false)
            return LoadingViewHolder(root)
        }


    }

    override fun getItemViewType(position: Int): Int {
        if (myDataset.get(position) is Loading)
            return R.layout.item_loading
        else
            return R.layout.item_git

    }

    fun addItems(data: List<Owner>) {
        myDataset.addAll(data)
        notifyDataSetChanged()
    }

    fun getItem(position: Int): Any {
        return myDataset.get(position)!!
    }

    fun getItems(): MutableList<Any?> {
        return myDataset
    }

    fun clearAll() {
        myDataset = ArrayList()
        notifyDataSetChanged()
    }

    fun addNullData() {
        myDataset!!.add(Loading())
        notifyDataSetChanged()
    }

    fun removeNull() {
        if (myDataset.size > 0){
            myDataset.remove(myDataset.get(myDataset.size - 1))
            notifyDataSetChanged()
        }

    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {

        if (holder is ViewHolder) {
            val item: Owner = myDataset[position] as Owner

            val tvName = holder.itemView.findViewById<TextView>(R.id.tvName)
            val image = holder.itemView.findViewById<CircularImageView>(R.id.image)

            tvName.text = item.login

            Picasso.get()
                .load(item.avatar_url)

                .centerCrop()
                .resize(100, 100)
                .into(image)

            setFadeAnimation(holder.itemView)
            if (onClickListener != null)
                holder.itemView.setOnClickListener {
                    onClickListener.OnClickItemListener(
                        holder.itemView,
                        position
                    )
                }
        }

    }

    private fun setFadeAnimation(view: View) {
        val anim = AlphaAnimation(0.0f, 1.0f)
        anim.duration = 800
        view.startAnimation(anim)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size

    class ViewHolder(itemView: View) : CustomViewHolder(itemView) {
        private val tvName: TextView = itemView.findViewById(R.id.tvName)
        private val image: CircularImageView = itemView.findViewById(R.id.image)

    }

    class LoadingViewHolder(itemView: View) : CustomViewHolder(itemView)

    open class CustomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}