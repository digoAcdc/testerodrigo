package com.rodrigo.testerodrigo.repository.favority

import android.annotation.SuppressLint
import com.rodrigo.testerodrigo.model.GitDAO
import com.rodrigo.testerodrigo.model.Owner

class FavorityRepositoryImplementation(var dao: GitDAO) : FavorityRepositoryInterface {
    @SuppressLint("CheckResult")
    override fun setGitFavority(
        owner: Owner,
        onComplete: () -> Unit,
        onError: (Throwable) -> Unit
    ) {

        try {
            var list = dao.getAllGits()
            var exist = false
            list.forEach {
                if (it.id == owner.id) {
                    exist = true
                }
            }

            if (exist)
                dao.deleteByUserId(owner.id)
            else
                dao.insertGit(owner)

            onComplete()
        } catch (e: Exception) {
            onError(Throwable("Falha ao favoritar"))
        }

    }

    override fun getListFavority(
        onComplete: (lista: List<Owner>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        try {
            onComplete(dao.getAllGits())
        } catch (e: Exception) {
            onError(Throwable("Falha retornar favoritos"))
        }

    }
}