package com.rodrigo.testerodrigo.repository.home

import com.rodrigo.testerodrigo.api.GitAPI
import com.rodrigo.testerodrigo.model.DataGit
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class ListGitRepositoryImplementation(var api: GitAPI) :
    ListGitRepositoryInterface {
    override fun getList(
        page: Int,
        onComplete: (lista: List<DataGit>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        api.publicList(page)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                //onError(Throwable("Falha ao carregar lista"))
            }
            .unsubscribeOn(Schedulers.io())
            .subscribe(object : Observer<List<DataGit>> {
                override fun onCompleted() {

                }

                override fun onError(e: Throwable?) {
                    onError(Throwable("Falha ao carregar lista"))

                }


                override fun onNext(list: List<DataGit>) {
                   onComplete(list)
                }

            })
    }
}