package com.rodrigo.testerodrigo.repository

import androidx.room.Database
import androidx.room.RoomDatabase
import com.rodrigo.testerodrigo.model.GitDAO
import com.rodrigo.testerodrigo.model.Owner

@Database(version = 1, entities = [Owner::class])
abstract class AppDataBase : RoomDatabase() {
    abstract fun gitDao(): GitDAO
}