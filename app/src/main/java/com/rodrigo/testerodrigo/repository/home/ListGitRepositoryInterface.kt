package com.rodrigo.testerodrigo.repository.home

import com.rodrigo.testerodrigo.model.DataGit

interface ListGitRepositoryInterface {
    fun getList(
        page:Int,
        onComplete: (lista:List<DataGit>) -> Unit,
        onError: (Throwable) -> Unit
    )
}