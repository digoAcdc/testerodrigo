package com.rodrigo.testerodrigo.repository.favority

import com.rodrigo.testerodrigo.model.Owner

interface FavorityRepositoryInterface {
    fun getListFavority(
        onComplete: (lista:List<Owner>) -> Unit,
        onError: (Throwable) -> Unit
    )

    fun setGitFavority(
        owner:Owner,
        onComplete: () -> Unit,
        onError: (Throwable) -> Unit
    )
}