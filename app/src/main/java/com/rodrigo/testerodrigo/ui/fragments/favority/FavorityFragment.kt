package com.rodrigo.testerodrigo.ui.fragments.favority

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.rodrigo.testerodrigo.R
import com.rodrigo.testerodrigo.adapter.GitAdapter
import com.rodrigo.testerodrigo.event.RefreshFavoriteEvent
import com.rodrigo.testerodrigo.model.Owner
import com.rodrigo.testerodrigo.model.Resource
import com.rodrigo.testerodrigo.model.ResourceStateEnum
import com.rodrigo.testerodrigo.ui.activity.DetailActivity
import kotlinx.android.synthetic.main.fragment_dashboard.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.koin.android.viewmodel.ext.android.viewModel

class FavorityFragment : Fragment() {

    val viewModel: FavorityViewModel by viewModel()
    lateinit var adapter: GitAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_dashboard, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = GitAdapter(onClickListener)
        rvFavority.adapter = adapter
        rvFavority.addItemDecoration(
            DividerItemDecoration(
                activity!!,
                DividerItemDecoration.VERTICAL
            )
        )
        rvFavority.layoutManager = LinearLayoutManager(context)


        viewModel.listFavority.observe(this, listFavorityObserver)
        viewModel.getFavoritys()
    }

    override fun onResume() {
        super.onResume()

        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this)
    }


    @Subscribe
    fun onEvent(event: RefreshFavoriteEvent) {
        if (event.isRefresh) {
            adapter.clearAll()
            viewModel.listFavority.observe(this, listFavorityObserver)
            viewModel.getFavoritys()
        }

    }

    var onClickListener = object : GitAdapter.GitItemOnClickListener {
        override fun OnClickItemListener(view: View, position: Int) {

            var i = Intent(activity, DetailActivity::class.java)
            i.putExtra("ITEM", adapter.getItem(position) as Owner)
            startActivity(i)
        }

    }

    private val listFavorityObserver = Observer<Resource<List<Owner>>> { result ->
        when (result.status) {
            ResourceStateEnum.LOADING -> {

            }
            ResourceStateEnum.ERROR -> {

            }
            ResourceStateEnum.SUCCESS -> {
                result.data?.let {
                    var listOwer: MutableList<Owner> = ArrayList()
                    it.forEach {
                        listOwer.add(it)
                    }

                    adapter.addItems(listOwer)
                }

            }
        }
    }
}