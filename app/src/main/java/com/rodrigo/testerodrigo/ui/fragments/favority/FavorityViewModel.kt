package com.rodrigo.testerodrigo.ui.fragments.favority


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import com.rodrigo.testerodrigo.model.Owner
import com.rodrigo.testerodrigo.model.Resource
import com.rodrigo.testerodrigo.user_case.favority.FavorityUserCaseInterface


class FavorityViewModel(var userCase: FavorityUserCaseInterface) : ViewModel() {


    var listFavority: MutableLiveData<Resource<List<Owner>>> = MutableLiveData()

    fun getFavoritys(){
        userCase.getListFavority({
            listFavority.value = Resource.success(it)
        },{
            listFavority.value = Resource.error(it.message)
        })
    }
}