package com.rodrigo.testerodrigo.ui.fragments.home


import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rodrigo.testerodrigo.R
import com.rodrigo.testerodrigo.adapter.GitAdapter
import com.rodrigo.testerodrigo.model.DataGit
import com.rodrigo.testerodrigo.model.Owner
import com.rodrigo.testerodrigo.model.Resource
import com.rodrigo.testerodrigo.model.ResourceStateEnum
import com.rodrigo.testerodrigo.ui.activity.DetailActivity
import com.rodrigo.testerodrigo.util.KeyboardUtil
import kotlinx.android.synthetic.main.fail.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.viewmodel.ext.android.viewModel


class ListHomeFragment : Fragment() {

    private var page = 0
    lateinit var adapter: GitAdapter
    val viewModel: ListHomeViewModel by viewModel()
    var isLoading = false
    var list: MutableList<Owner> = ArrayList()
    var searchList: MutableList<Owner> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        adapter = GitAdapter(onClickListener)
        rvGit.adapter = adapter
        rvGit.addItemDecoration(
            DividerItemDecoration(
                activity!!,
                DividerItemDecoration.VERTICAL
            )
        )
        rvGit.layoutManager = LinearLayoutManager(context)
        isLoading = true
        viewModel.list.observe(this, listObserver)
        viewModel.getList(page)

        swipe_refresh_layout.setOnRefreshListener {
            fail.visibility = View.GONE
            page = 0
            adapter.clearAll()
            viewModel.list.observe(this, listObserver)
            viewModel.getList(page)
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(texto: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(texto: String?): Boolean {
                searchList = ArrayList()
                if (texto?.trim().isNullOrEmpty()) {
                    adapter.clearAll()
                    adapter.addItems(list)
                } else {


                    adapter.getItems().forEach {


                        if(it is Owner){

                            if (it.login.startsWith(texto!!))
                                searchList.add(it)
                        }


                    }

                    adapter.clearAll()
                    adapter.addItems(searchList)
                }
                return true

            }
        })

        KeyboardUtil.hideKeyboard(activity!!)


    }

    var onClickListener = object : GitAdapter.GitItemOnClickListener {
        override fun OnClickItemListener(view: View, position: Int) {

            var i = Intent(activity, DetailActivity::class.java)
            i.putExtra("ITEM", adapter.getItem(position) as Owner)

            startActivity(i)
        }

    }


    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.search, menu);
        super.onCreateOptionsMenu(menu, inflater)
    }

    fun loadMore() {

        if (!isLoading) {
            adapter.addNullData()
            page += 1
            viewModel.list.observe(this, listObserver)
            viewModel.getList(page)
        }

    }

    private val listObserver = Observer<Resource<List<DataGit>>> { result ->
        when (result.status) {
            ResourceStateEnum.LOADING -> {
                isLoading = true
            }
            ResourceStateEnum.ERROR -> {
                fail.visibility = View.VISIBLE
                swipe_refresh_layout.isRefreshing = false
                isLoading = false
                adapter.removeNull()

            }
            ResourceStateEnum.SUCCESS -> {
                swipe_refresh_layout.isRefreshing = false
                isLoading = false
                adapter.removeNull()



                result.data?.let {
                    var listOwer: MutableList<Owner> = ArrayList()
                    it.forEach {
                        listOwer.add(it.owner)
                    }
                    list.addAll(listOwer)
                    adapter.addItems(listOwer)
                }


                rvGit.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                        if (!recyclerView.canScrollVertically(1) && dy != 0) {

                            loadMore()
                        }
                    }
                })

            }
        }
    }
}