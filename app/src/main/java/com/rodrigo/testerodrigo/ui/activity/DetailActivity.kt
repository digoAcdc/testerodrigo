package com.rodrigo.testerodrigo.ui.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.rodrigo.testerodrigo.R
import com.rodrigo.testerodrigo.event.RefreshFavoriteEvent
import com.rodrigo.testerodrigo.model.Owner
import com.rodrigo.testerodrigo.model.Resource
import com.rodrigo.testerodrigo.model.ResourceStateEnum
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail.*
import org.greenrobot.eventbus.EventBus
import org.koin.android.viewmodel.ext.android.viewModel
import com.google.android.material.snackbar.Snackbar




class DetailActivity : AppCompatActivity() {

    lateinit var owner: Owner

    val viewModel: DetailActivityViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        var extras = intent.extras

        if (extras != null) {
            owner = extras.getParcelable("ITEM")!!

            tvName.text = owner.login

            Picasso.get()
                .load(owner.avatar_url)
                .centerCrop()
                .resize(100, 100)
                .into(image)

        }

    }




    private val setFavorityObserver = Observer<Resource<Void>> { result ->
        when (result.status) {
            ResourceStateEnum.LOADING -> {

            }
            ResourceStateEnum.ERROR -> {
                val snackbar = Snackbar
                    .make(container, "Falha ao realizar operação", Snackbar.LENGTH_LONG)
                snackbar.show()
            }
            ResourceStateEnum.SUCCESS -> {
                val snackbar = Snackbar
                    .make(container, "Operação realizada com sucesso", Snackbar.LENGTH_LONG)
                snackbar.show()
                EventBus.getDefault().post(RefreshFavoriteEvent(true))

            }
        }
    }




    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.favority, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.favorite -> {
                viewModel.favority.observe(this, setFavorityObserver)
                viewModel.setFavority(owner)
            }
        }

        return true
    }
}
