package com.rodrigo.testerodrigo.ui.fragments.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rodrigo.testerodrigo.model.DataGit
import com.rodrigo.testerodrigo.model.Resource
import com.rodrigo.testerodrigo.user_case.home.ListGitUserCaseInterface

class ListHomeViewModel(var userCase: ListGitUserCaseInterface) : ViewModel() {

    var list: MutableLiveData<Resource<List<DataGit>>> = MutableLiveData()

    fun getList(page: Int) {

        list.value = Resource.loading()

        userCase.getList(page, {
            list.value = Resource.success(it)
        }, {
            list.value = Resource.error(it.message)
        })
    }
}