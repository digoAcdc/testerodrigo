package com.rodrigo.testerodrigo.ui.activity

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rodrigo.testerodrigo.model.Owner
import com.rodrigo.testerodrigo.model.Resource
import com.rodrigo.testerodrigo.user_case.detail.DetailActivityUserCaseInterface

class DetailActivityViewModel(val userCaseInterface: DetailActivityUserCaseInterface) :
    ViewModel() {

    var favority: MutableLiveData<Resource<Void>> = MutableLiveData()

    fun setFavority(owner: Owner) {
        userCaseInterface.setGitFavority(owner, {
            favority.value = Resource.success()
        }, {
            favority.value = Resource.error(it.message)
        })
    }

}