package com.rodrigo.testerodrigo

import android.app.Application
import com.rodrigo.testerodrigo.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MyApplication: Application() {

    /*companion object {
        var database: AppDataBase? = null
    }*/

    override fun onCreate() {
        super.onCreate()

        //database = Room.databaseBuilder(this, AppDataBase::class.java, "git-db").allowMainThreadQueries().build()


        startKoin {
            androidLogger()
            androidContext(this@MyApplication)
            modules(viewModelModule, repository, networkModule, userCase, dbModule)

        }
    }
}