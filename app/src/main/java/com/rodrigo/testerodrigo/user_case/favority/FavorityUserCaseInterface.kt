package com.rodrigo.testerodrigo.user_case.favority

import com.rodrigo.testerodrigo.model.Owner

interface FavorityUserCaseInterface {
    fun getListFavority(
        onComplete: (lista:List<Owner>) -> Unit,
        onError: (Throwable) -> Unit
    )
}