package com.rodrigo.testerodrigo.user_case.detail

import com.rodrigo.testerodrigo.model.Owner

interface DetailActivityUserCaseInterface {

    fun setGitFavority(
        owner: Owner,
        onComplete: () -> Unit,
        onError: (Throwable) -> Unit
    )
}