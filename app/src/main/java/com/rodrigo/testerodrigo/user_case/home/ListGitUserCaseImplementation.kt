package com.rodrigo.testerodrigo.user_case.home

import com.rodrigo.testerodrigo.model.DataGit
import com.rodrigo.testerodrigo.repository.home.ListGitRepositoryInterface

class ListGitUserCaseImplementation(var respository: ListGitRepositoryInterface):
    ListGitUserCaseInterface {
    override fun getList(
        page: Int,
        onComplete: (lista: List<DataGit>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        respository.getList(page,
            {
                onComplete(it)
            },{
                onError(it)
            })
    }
}