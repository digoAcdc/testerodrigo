package com.rodrigo.testerodrigo.user_case.detail

import com.rodrigo.testerodrigo.model.Owner
import com.rodrigo.testerodrigo.repository.favority.FavorityRepositoryInterface

class DetailActivityUserCaseImplementation(var repositoryInterface: FavorityRepositoryInterface):DetailActivityUserCaseInterface {
    override fun setGitFavority(
        owner: Owner,
        onComplete: () -> Unit,
        onError: (Throwable) -> Unit
    ) {
        repositoryInterface.setGitFavority(owner,{
            onComplete()
        },{
            onError(it)
        })
    }
}