package com.rodrigo.testerodrigo.user_case.favority

import com.rodrigo.testerodrigo.model.Owner
import com.rodrigo.testerodrigo.repository.favority.FavorityRepositoryInterface

class FavorityUserCaseImplementation(val repository: FavorityRepositoryInterface): FavorityUserCaseInterface{
    override fun getListFavority(
        onComplete: (lista: List<Owner>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        repository.getListFavority({
            onComplete(it)
        },{
            onError(it)
        })
    }
}