package com.rodrigo.testerodrigo.user_case.home

import com.rodrigo.testerodrigo.model.DataGit

interface ListGitUserCaseInterface {
    fun getList(
        page:Int,
        onComplete: (lista:List<DataGit>) -> Unit,
        onError: (Throwable) -> Unit
    )
}