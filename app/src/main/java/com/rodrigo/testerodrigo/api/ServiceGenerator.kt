package com.rodrigo.testerodrigo.api

import android.content.Context

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.rodrigo.testerodrigo.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

import java.util.concurrent.TimeUnit

class ServiceGenerator {



    companion object {

        private val httpClient = OkHttpClient.Builder()
        private var context: Context? = null

        fun <S> createService(serviceClass: Class<S>): S {

            return generateService(serviceClass)
        }


        private fun <S> generateService(serviceClass: Class<S>): S {

            val builder = Retrofit.Builder()
                .baseUrl(BuildConfig.URLAPIGIT)
                .addConverterFactory(GsonConverterFactory.create())

            val gsonBuilder = GsonBuilder()
            gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            gsonBuilder.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")

            val client = httpClient
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build()


            builder.client(client)
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())



            return builder.build().create(serviceClass)
        }
    }

}