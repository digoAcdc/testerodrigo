package com.rodrigo.testerodrigo.api

import com.rodrigo.testerodrigo.model.DataGit
import retrofit2.http.GET

import retrofit2.http.Query
import rx.Observable

interface GitAPI {
    @GET("/gists/public")
    fun publicList(@Query("page")  page: Int) : Observable<List<DataGit>>
}