package com.rodrigo.testerodrigo.di

import androidx.room.Room
import com.rodrigo.testerodrigo.api.GitAPI
import com.rodrigo.testerodrigo.api.ServiceGenerator
import com.rodrigo.testerodrigo.repository.AppDataBase
import com.rodrigo.testerodrigo.repository.favority.FavorityRepositoryImplementation
import com.rodrigo.testerodrigo.repository.favority.FavorityRepositoryInterface
import com.rodrigo.testerodrigo.repository.home.ListGitRepositoryImplementation
import com.rodrigo.testerodrigo.repository.home.ListGitRepositoryInterface
import com.rodrigo.testerodrigo.ui.activity.DetailActivityViewModel
import com.rodrigo.testerodrigo.ui.fragments.favority.FavorityViewModel
import com.rodrigo.testerodrigo.ui.fragments.home.ListHomeViewModel
import com.rodrigo.testerodrigo.user_case.detail.DetailActivityUserCaseImplementation
import com.rodrigo.testerodrigo.user_case.detail.DetailActivityUserCaseInterface
import com.rodrigo.testerodrigo.user_case.favority.FavorityUserCaseImplementation
import com.rodrigo.testerodrigo.user_case.favority.FavorityUserCaseInterface
import com.rodrigo.testerodrigo.user_case.home.ListGitUserCaseImplementation
import com.rodrigo.testerodrigo.user_case.home.ListGitUserCaseInterface
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val viewModelModule: Module = module {
    viewModel { ListHomeViewModel(get()) }
    viewModel { FavorityViewModel(get()) }
    viewModel { DetailActivityViewModel(get()) }
}

val userCase: Module = module {
    single<ListGitUserCaseInterface> { ListGitUserCaseImplementation(get())}
    single<FavorityUserCaseInterface> { FavorityUserCaseImplementation(get())}
    single<DetailActivityUserCaseInterface> { DetailActivityUserCaseImplementation(get())}
}

val repository: Module = module {
    single<ListGitRepositoryInterface> { ListGitRepositoryImplementation(get())}
    single<FavorityRepositoryInterface> { FavorityRepositoryImplementation(get())}
}

val dbModule: Module = module {
    single { Room.databaseBuilder(get(), AppDataBase::class.java, "git-db").allowMainThreadQueries().build() }
    single { get<AppDataBase>().gitDao() }
}


val networkModule: Module = module {
    factory { getListGit() }
}


fun getListGit(): GitAPI {
    return ServiceGenerator.createService(
        GitAPI::class.java)
}